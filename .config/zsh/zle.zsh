# Commands:
# Create a new user-defined widget:
#   zle -N widget [function]        'function' defaults to the same name as the
#                                   widget
# Select a specific keymap for the current command:
#   bindkey -M keymap ...
#
# Create a new keymap, if oldmap is given then initialize newmap with it:
#   bindkey -N newmap [oldmap]

# Updates editor information when the keymap changes.
function zle-keymap-select() {
  # update keymap variable for the prompt
  VI_KEYMAP=$KEYMAP

  zle reset-prompt
  zle -R
}
zle -N zle-keymap-select

# use custom accept-line widget to update $VI_KEYMAP
function vi-accept-line() {
  VI_KEYMAP=main
  zle accept-line
}
zle -N vi-accept-line
bindkey -M vicmd '^J' vi-accept-line

# allow v to edit the command line (standard behaviour)
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M vicmd 'v' edit-command-line

# allow ctrl-j to accept the current completion and continue completion
bindkey '^j' accept-and-infer-next-history

# allow ctrl-p, ctrl-n for navigate history (standard behaviour)
bindkey '^P' up-history
bindkey '^N' down-history

# allow ctrl-h, ctrl-w, ctrl-? for char and word deletion (standard behaviour)
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey '^w' backward-kill-word

# allow ctrl-r and ctrl-s to search the history
bindkey '^r' history-incremental-search-backward
bindkey '^s' history-incremental-search-forward

# allow ctrl-a and ctrl-e to move to beginning/end of line
bindkey '^a' beginning-of-line
bindkey '^e' end-of-line
# allow alt-f and alt-b to move forward/backward by word
bindkey '^[b' vi-backward-word
bindkey '^[f' vi-forward-word

bindkey '^k' kill-line

# abort the entire line
bindkey '^u' vi-change-whole-line
# stop menu completion
bindkey '^g' send-break
# manually trigger menu completion
bindkey '^[[Z' menu-expand-or-complete

# Move down by line in the completion menu
bindkey -M menuselect '^[m' down-line-or-history
# Move down by page in the completion menu
bindkey -M menuselect '^[.' forward-char
# Move up by line in the completion menu
bindkey -M menuselect '^[,' up-line-or-history
# Move up by page in the completion menu
bindkey -M menuselect '^[n' backward-char
bindkey -M menuselect '^[j' accept-and-menu-complete

# Remove '/' and '.' from WORDCHARS to prevent backword-delete-word from
# deleting an entire path or the basename of a file name
WORDCHARS='*?_-[]~=&;!#$%^(){}<>'
#   vim:ft=zsh:tw=80
