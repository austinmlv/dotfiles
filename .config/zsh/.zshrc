setopt autocd
autoload -U promptinit
promptinit
zstyle ':completion:*' menu select
# load completion menu colors
zmodload zsh/complist
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
source ~/.config/zsh/zle.zsh
# My zocket setup:
zocket begin
zocket github belak/zsh-utils file completion/completion.plugin.zsh
zocket github belak/zsh-utils file history/history.plugin.zsh
zocket github zsh-users/zsh-syntax-highlighting
zocket github zuxfoucault/colored-man-pages_mod
zocket github sindresorhus/pure file async.zsh
source "$ZOCKET_PATH"/zocket.zsh

source ~/.config/shell/rc
#   vim: ft=zsh:tw=80
