(setq-default custom-file null-device)

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; enable fuzzy completion in minibuffer:
;; emacs manual: 8.4.4
;; (setq completion-styles '(... flex ...))
;;
;; enable cycling of completion candidates:
;; emacs manual 8.4.5
;; (setq completion-cycle-threshold [t|NUMBER])

;; install and initialize use-package:
(require 'package)
(setq package-archives '(("melpa-unstable" . "https://melpa.org/packages/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

;; appearance:

(use-package doom-themes
  :init
  (setq doom-themes-enable-bold t
	doom-themes-enable-italic t)
  (load-theme 'doom-one t))

;; NOTE: for mode line configuration look up the variable 'mode-line-format'
(use-package doom-modeline
  :ensure t
  :init (setq doom-modeline-major-mode-icon nil
	      doom-modeline-height 18
	      doom-modeline-buffer-encoding nil
	      doom-modeline-minor-modes t
	      doom-modeline-buffer-modification-icon nil
	      doom-modeline-buffer-file-name-style 'relative-from-project)
  :hook (after-init . doom-modeline-mode))

(use-package highlight-parentheses
  :hook ((prog-mode) . highlight-parentheses-mode))

(use-package rainbow-delimiters
  :hook ((prog-mode) . rainbow-delimiters-mode))

(use-package which-key
  :init
  (setq which-key-show-early-on-C-h t
	which-key-idle-delay 10000
	which-key-idle-secondary-delay 0.05)
  (which-key-setup-side-window-right-bottom)
  (which-key-mode))

;; behaviour:

(use-package evil :ensure t
  :init
  (setq evil-want-keybinding nil
	evil-want-integration t
	evil-split-window-below t
	evil-split-window-right t)
  :config
  (evil-mode))

(use-package evil-collection :ensure t
  :after evil
  :config
  (evil-collection-init))

(use-package paredit
  :hook ((emacs-lisp-mode scheme-mode) . paredit-mode))

(use-package geiser)
(use-package geiser-guile)

(use-package org)

;; Evil mode key bindings
(evil-define-key 'normal 'global (kbd "M-h") 'evil-scroll-page-up)
(evil-define-key 'normal 'global (kbd "M-j") 'evil-scroll-down)
(evil-define-key 'normal 'global (kbd "M-k") 'evil-scroll-up)
(evil-define-key 'normal 'global (kbd "M-l") 'evil-scroll-page-down)
(evil-define-key 'normal 'global (kbd "M-J") 'evil-window-up)
(evil-define-key 'normal 'global (kbd "M-J") 'evil-window-down)
(evil-define-key 'normal 'global (kbd "M-H") 'evil-window-left)
(evil-define-key 'normal 'global (kbd "M-L") 'evil-window-right)
