(require-macros :macros)

(g= :mapleader " ")
(g= :maplocalleader "\\")

(fn defkey [mode keys target]
  (vim.keymap.set mode keys target {:noremap true}))

(fn leader [mode keys target]
  (vim.keymap.set mode (.. "<leader>" keys) target {:noremap true}))

(fn defcmd [mode keys target]
  (vim.keymap.set mode keys (.. ":" target "<cr>") {:noremap true}))

(leader [:n :v] "fee" ":Fnl<cr>")
(leader :n "fef" ":Fnlfile %<cr>")

;; Config file access
(let [nvim-home (.. (vim.fn.stdpath :config) "/fnl")
      edit (fn [path] (.. ":edit " nvim-home "/" path "<cr>"))]
  (leader :n "vi" (edit ""))
  (leader :n "vp" (edit "plugins"))
  (leader :n "vk" (edit "keys.fnl"))
  (leader :n "vs" (edit "settings.fnl")))

;; Telescope
(assert-plugin
  :telescope.nvim
  (let [tb (require :telescope.builtin)]
    (leader :n "sa" #(tb.find_files {:no_ignore true :hidden true}))
    (leader :n "sb" tb.buffers)
    (leader :n "sc" tb.colorscheme)
    (leader :n "sf" tb.find_files)
    (leader :n "sg" tb.live_grep)
    (leader :n "sh" #(tb.find_files {:hidden true}))
    (leader :n "si" #(tb.find_files {:no_ignore true}))
    (leader :n "st" tb.builtin)
    (leader :n "sm" #(tb.man_pages {:sections ["ALL"]}))
    (leader
      :n
      "sp"
      #(vim.ui.input
         {:prompt "Path: "
          :completion "file"}
         #(and $1 (tb.find_files {:cwd $1}))))))

;; LSP
(fn set-lsp-keys []
  (let [key (fn [keys action]
              (vim.keymap.set :n keys action {:buffer true :noremap true}))]
    (key "K" vim.lsp.buf.hover)
    (key "gd" vim.lsp.buf.definition)
    (key "gD" vim.lsp.buf.declaration)
    (key "gr" vim.lsp.buf.references)
    (key "<c-k>" vim.lsp.buf.signature_help)
    (key "<f2>" vim.lsp.buf.rename)
    (key "<f4>" vim.lsp.buf.code_action)
    (key "gl" vim.diagnostic.open_float)
    (key "[d" vim.diagnostic.goto_prev)
    (key "]d" vim.diagnostic.goto_next)
    (key "gi" vim.lsp.buf.incoming_calls)
    (assert-plugin
      :nvim-cmp
      (vim.keymap.set
        :i "<c-x><c-o>"
        "<cmd>lua require('cmp').complete()<cr>"
        {:buffer true})
      (vim.keymap.set
        :i "<c-s>" "<cmd>LspOverloadsSignature<cr>" {:noremap true :silent true}))))

(vim.api.nvim_create_autocmd
  :LspAttach
  {:desc "Set LSP key bindings"
   :callback set-lsp-keys})

;; Neovim
(defcmd :n "ZV" "qall")

;; Tabs
(defcmd :n "<a-u>" "tabp")
(defcmd :n "<a-i>" "tabn")

;; Windows
;; Movement
(defkey :n "<leader>wh" "<c-w>h")
(defkey :n "<leader>wk" "<c-w>k")
(defkey :n "<leader>wj" "<c-w>j")
(defkey :n "<leader>wl" "<c-w>l")
(defkey :t "<s-a-h>" "<c-\\><c-n><c-w>h")
(defkey :t "<s-a-j>" "<c-\\><c-n><c-w>j")
(defkey :t "<s-a-k>" "<c-\\><c-n><c-w>k")
(defkey :t "<s-a-l>" "<c-\\><c-n><c-w>l")
;; Close current window
(defkey :n "<leader>cw" "<c-w>c")
;; Close other windows
(defkey :n "<leader>co" "<c-w>o")
;; Close location / preview window
(defcmd :n "<leader>cl" "lclose | pclose | cclose")

;; Buffers
(defcmd :n "<a-n>" "bn")
(defcmd :n "<a-p>" "bp")
;; Scrolling
(defkey :n "<a-h>" "<c-b>")
(defkey :n "<a-k>" "<c-u>")
(defkey :n "<a-j>" "<c-d>")
(defkey :n "<a-l>" "<c-f>")
(defkey :n "<a-,>" "<c-y>")
(defkey :n "<a-m>" "<c-e>")

(defcmd :n "<leader>]" "lnext | silent! cnext")
(defcmd :n "<leader>[" "lprev | silent! cprev")

(defkey :t "<s-a-n>" "<c-\\><c-n>")
;; Settings
;; Toggle spelling
(defcmd :n "<leader>k" "setlocal spell!")

(leader :n "xt" "<plug>(ScratchToggle)")
(leader :n "xg" "<plug>(ScratchGoto)")
