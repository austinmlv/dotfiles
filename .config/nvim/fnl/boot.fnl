(fn ensure-packer []
  (let [packer-path (.. (vim.fn.stdpath "data")
                        "/site/pack/packer/start/packer.nvim")]
    (if (> (vim.fn.empty (vim.fn.glob packer-path)) 0)
      (do
        (vim.api.nvim_out_write
          (.. "Could not find packer.nvim; cloning to " packer-path "\n"))
        (vim.fn.system ["git"
                        "clone"
                        "https://github.com/wbthomason/packer.nvim"
                        packer-path])
        (vim.cmd "packadd packer.nvim")))))

(fn setup []
  (ensure-packer)
  (let [settings (require :settings)]
    (require :plugins)
    (require :keys)
    (require :commands)
    (settings.sync-colorscheme)
    (vim.notify "press any key to begin hacking...")))

{: setup}
