(fn setup []
  (let [telescope (require :telescope)
        sorters (require :telescope.sorters)
        actions (require :telescope.actions)
        borderchars ["─" "│" "─" "│" "┌" "┐" "┘" "└"]]
    (telescope.setup
      {:defaults {: borderchars
                  :preview {:check_mime_type true}
                  :file_sorter sorters.get_fzy_sorter
                  :layout_strategy "vertical"
                  :layout_config {:width 0.90}
                  :mappings {:i {"<C-u>" false
                                 "<a-k>" actions.move_selection_previous
                                 "<a-j>" actions.move_selection_next}}}
       :pickers {:find_files {:follow true}
                 :theme "ivy"}})))

{: setup}
