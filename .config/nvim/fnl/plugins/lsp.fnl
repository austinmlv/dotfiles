(require-macros :macros)

(fn lspconfig-setup []
  (let [cmp (require :cmp)
        lspconfig (require :lspconfig)
        customized {:flags {:debounce_text_changes 1000}
                    :capabilities (call :cmp_nvim_lsp :default_capabilities)}]
    (tset
      lspconfig.util
      :default_config (vim.tbl_deep_extend
                        :force
                        lspconfig.util.default_config
                        customized))

    (let [disable-group (vim.api.nvim_create_augroup :lsp_disable {:clear true})]
      (vim.api.nvim_create_autocmd
        [:BufEnter :BufNewFile]
        {:group disable-group
         :pattern ["conjure-log-*" "*.nimble"]
         :callback (fn [] (vim.diagnostic.enable false {:bufnr 0}))}))
    (lspconfig.lua_ls.setup
      {:diagnostics {:globals ["vim"]}
       :telemetry {:enable false}})
    (lspconfig.fennel_ls.setup
      {:root_dir (fn [dir]
                   (or (lspconfig.util.find_git_ancestor dir)
                       (vim.fn.getcwd)))})
    (lspconfig.nimls.setup {})))

(fn cmp-setup []
  (let [cmp (require :cmp)
        expand-fn (fn [args] (call :luasnip :lsp_expand args.body))]
    (cmp.setup
      {:completion {:autocomplete false}
       :snippet {:expand expand-fn}
       :mapping (cmp.mapping.preset.insert
                  {:<c-d> (cmp.mapping.scroll_docs -4)
                   :<c-f> (cmp.mapping.scroll_docs 4)
                   :<c-space> (cmp.mapping.complete)
                   :<cr> (cmp.mapping.confirm {:select true})})
       :sources (cmp.config.sources
                  [{:name "nvim_lsp"}
                   {:name "luasnip"}
                   {:name "buffer"}])})))

{: lspconfig-setup
 : cmp-setup}
