(require-macros :macros)
(local packer (require :packer))

(fn use [path ...]
  (fn list->table [list]
    (var result {})
    (for [i 1 (length list) 2]
      (tset result (. list i) (. list (+ 1 i))))
    result)

  (local args {1 path})
  (var vargs {})
  (when ...
    (set vargs (list->table [...])))
  (packer.use (vim.tbl_extend :error args vargs)))

(packer.init)
(packer.reset)

(use "wbthomason/packer.nvim")
(use "rktjmp/hotpot.nvim")

;; UX
(use "bradcush/nvim-base16")

(use "vim-airline/vim-airline"
      :requires "vim-airline/vim-airline-themes"
      :config ($ :plugins.airline :setup))

(use "folke/todo-comments.nvim"
      :config (fn []
                (call
                  :todo-comments
                  :setup
                  {:highlight {:before :empty :keyword :fg :after :empty}})))

(use "nvim-telescope/telescope.nvim"
     :requires ["nvim-lua/popup.nvim"
                "nvim-lua/plenary.nvim"
                "nvim-telescope/telescope-symbols.nvim"]
     :config ($ :plugins.telescope :setup))

(use "nvim-treesitter/nvim-treesitter"
     :requires ["nvim-treesitter/playground" "HiPhish/rainbow-delimiters.nvim"]
     :config ($ :plugins.treesitter :setup)
     :run ":TSUpdateSync")

(use "hrsh7th/nvim-cmp"
      :requires ["hrsh7th/cmp-nvim-lsp"
                 "hrsh7th/cmp-buffer"
                 "hrsh7th/cmp-path"
                 "hrsh7th/cmp-cmdline"
                 "L3MON4D3/LuaSnip"
                 "saadparwaiz1/cmp_luasnip"
                 "PaterJason/cmp-conjure"]
      :config ($ :plugins.lsp :cmp-setup))

(use "neovim/nvim-lspconfig"
     :config ($ :plugins.lsp :lspconfig-setup))

(use "tpope/vim-fugitive")

;; Editing
(use "ggandor/leap.nvim"
     :requires "tpope/vim-repeat"
     :config (fn [] (call :leap :add_default_mappings)))
(use "guns/vim-sexp"
     :config ($ :plugins.sexp :setup))
(use "kylechui/nvim-surround" :config (fn [] (call :nvim-surround :setup)))
(use "jiangmiao/auto-pairs" :ft :ocaml)
(use "austinmlv/conjure"
     :branch "multiple-result"
      ; "Olical/conjure"
      :config (fn []
                (g= :conjure#log#hud#border "none")
                (g= :conjure#filetype#scheme "conjure.client.guile.socket")
                (g= :conjure#client#guile#socket#pipename "repl")
                (g= :conjure#filetype#ocaml "conjure.client.ocaml.utop")))
(use "sbdchd/neoformat"
     :config (fn []
               (g= :neoformat_enabled_nim [:nph])))
;; Filetypes
(use "austinmlv/fennel.vim"
     :branch "dev")
(use "editorconfig/editorconfig-vim")
(use "alaviss/nim.nvim")
(use
  "https://gitlab.com/HiPhish/info.vim"
  :config
  (fn []
    (fn set-info-keys []
      (fn pmap [action]
        (table.concat ["<Plug>(Info" action ")"]))
      (fn key [keys action]
        (vim.keymap.set :n keys (pmap action) {:buffer true :noremap true}))
      (key "M" "Menu")
      (key "P" "Prev")
      (key "N" "Next")
      (key "U" "Up")
      (key "F" "Follow")
      (key "T" "Goto"))
    (vim.api.nvim_create_autocmd
      :FileType
      {:desc "Set Info key bindings"
       :pattern "info"
       :callback set-info-keys})))
(let [opam-share (-> (vim.fn.system "opam var share")
                     (string.gsub "\n" ""))]
  (assert-plugin
    :merlin
    (use (.. opam-share "/merlin/vim")
         :as "merlin"
         :setup (fn [] (g= :no_ocaml_maps 1))
         :run (fn [] (vim.cmd.helptags (.. opam-share "/merlin/vim/doc"))))))
