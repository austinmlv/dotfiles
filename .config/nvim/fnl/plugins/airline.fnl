(require-macros :macros)

(fn setup []
  (g= :airline_powerline_fonts true)
  (g= :airline_detect_modified true)
  (g= :airline_detect_crypt true)
  (g= :airline_inactive_collapse true)
  (g= :airline_mode_map {:__ "-" :c "C"
                         :i "I" :ic "I"
                         :ix "I" :n "N"
                         :multi "M" :ni "N"
                         :no "N" :R "R"
                         :Rv "R" :s "S"
                         :S "S" :^S "S"
                         :t "T" :v "V"
                         :V "V" :^V "V"})
  (g= :airline#extensions#default#layout [["a" "b" "c"] ["x" "z"]])
  (g= :airline_left_sep "")
  (g= :airline_right_sep "")
  (g= :airline_section_z "%P %l/%L:%c%V")
  (g= :airline#extensions#tabline#enabled true)
  (g= :airline#extensions#tabline#show_splits false)
  (g= :airline#extensions#tabline#show_buffers true)
  (g= :airline#extensions#tabline#tab_nr_type true)
  (g= :airline#extensions#tabline#show_close_button false)
  (g= :airline#extensions#tabline#formatter "unique_tail")
  (g= :airline#extensions#tabline#fnamecollapse true)
  (g= :airline#extensions#branch#enabled true)
  (g= :airline#extensions#branch#empty_message "")
  (g= :airline#extensions#fugitiveline#enabled true)
  (g= :airline#extensions#gutentags#enabled true)
  (g= :airline#extensions#term#enabled true))

{: setup}
