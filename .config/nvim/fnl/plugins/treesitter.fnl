(require-macros :macros)

(fn setup []
  (let [treesitter (require :nvim-treesitter.configs)
        parsers (require :nvim-treesitter.parsers)
        languages ["bash" "c" "cpp" "fennel" "haskell" "javascript" "json"
                   "lua" "make" "markdown" "nim" "ocaml" "query" "scheme"
                   "toml" "vim" "vimdoc" "yaml" "zig"]
        rainbow-delimiters (require :rainbow-delimiters)]
    (treesitter.setup
      {:highlight {:enable true
                   :additional_vim_regex_highlighting ["scheme" "fennel"]}
       :ensure_installed languages})
    (g= :rainbow_delimiters
        {:strategy {"" rainbow-delimiters.strategy.global
                    :vim rainbow-delimiters.strategy.local}
         :query {"" :rainbow-delimiters
                 :lua :rainbow-blocks}
         :blacklist ["nim"]})
    (<- (vim.cmd)
        "hi! link RainbowDelimiterRed @keyword"
        "hi! link RainbowDelimiterOrange @function"
        "hi! link RainbowDelimiterYellow @constructor"
        "hi! link RainbowDelimiterGreen @type"
        "hi! link RainbowDelimiterCyan @string"
        "hi! link RainbowDelimiterBlue @number"
        "hi! link RainbowDelimiterViolet @text")))

{: setup}
