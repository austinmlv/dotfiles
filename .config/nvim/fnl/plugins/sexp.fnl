(require-macros :macros)

(fn setup []
  (g= :sexp_filetypes "scheme,fennel,lisp,clojure")
  (g= :sexp_insert_after_wrap false)
  (set vim.g.sexp_mappings
       {:sexp_swap_element_backward "<m-s>"
        :sexp_swap_element_forward "<m-d>"
        :sexp_swap_list_backward "<m-a>"
        :sexp_swap_list_forward "<m-f>"}))

{: setup}
