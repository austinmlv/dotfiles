(fn set+ [name]
  `(tset vim.o ,name true))

(fn set- [name]
  `(tset vim.o ,name false))

(fn set= [name value]
  `(tset vim.opt ,name ,value))

(fn set+= [name value]
  `(: (. vim.opt ,name) :append ,value))

(fn set^= [name value]
  `(: (. vim.opt ,name) :prepend ,value))

(fn set-= [name value]
  `(: (. vim.opt ,name) :remove ,value))

(fn set! [name]
  `(vim.cmd (.. "set " ,name "!")))

(fn g= [name value]
  `(tset vim.g ,name ,value))

(fn $ [mod name ...]
  `(. (require ,mod) ,name ,...))

(fn call [mod name ...]
  `((. (require ,mod) ,name) ,...))

(fn setup [mod ...]
  `((. (require ,mod) :setup) ,...))

(fn for-each [f xs]
  "Call function f on each element of the list xs."
  "If xs is a table then call f on each key-value pair."
  (assert (or (sequence? xs)
              (table? xs))
          "second parameter must be a list")
  (if (sequence? xs)
    `(each [_# value# (ipairs ,xs)]
       (,f value#))
    `(each [key# value# (pairs ,xs)]
       (,f key# value#))))

(fn map [f xs]
  "Returns a list where each element is the result of calling function f on
  each element of the list xs."
  "If xs is a table then call f on each key-value pair."
  (assert (or (sequence? xs)
              (table? xs))
          "second parameter must be a list")
  (if (sequence? xs)
    `(let [out# []]
       (each [_# value# (ipairs ,xs)]
         (table.insert out# (,f value#)))
       out#)
    `(let [out# {}]
       (each [key# value# (pairs ,xs)]
         (table.insert out# (,f key# value#)))
       out#)))

(fn slurp [form ...]
  "Insert each element of ... into form."
  (fn copy [t]
    (let [out []]
      (each [_ v (ipairs t)] (table.insert out v))
      (setmetatable out (getmetatable t))))
  (var out (list `do))
  (each [_ element (ipairs [...])]
    (var duplicate (copy form))
    (table.insert duplicate element)
    (table.insert out duplicate))
  out)

(fn assert-plugin [name body ...]
  `(let [plugins-start# (.. ($ :packer :config :package_root) "/packer/start")
         plugins-opt# (.. ($ :packer :config :package_root) "/packer/opt")
         start-result# (vim.fn.finddir ,name plugins-start#)
         opt-result# (vim.fn.finddir ,name plugins-opt#)]
     (if (and (= "" start-result#) (= "" opt-result#))
       nil
       (do
         ,body
         ,...))))

{: set+
 : set-
 : set=
 : set+=
 : set^=
 : set-=
 : set!
 : g=
 : $
 : call
 : for-each
 : map
 :<- slurp
 : assert-plugin}
