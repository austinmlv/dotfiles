(require-macros :macros)

(vim.api.nvim_create_user_command
  :DeleteTrailingSpace
  (fn [opts]
    (let [save-cursor (vim.fn.getpos ".")]
      (vim.cmd "silent % s/\\s\\+$//e")
      (vim.fn.setpos "." save-cursor)))
  {})

(vim.api.nvim_create_autocmd
  [:BufWritePre]
  {:command "DeleteTrailingSpace"})

(vim.api.nvim_create_user_command
  :SyncColorscheme
  (fn []
    (call :settings :sync-colorscheme)
    (let [theme-name (-> (vim.api.nvim_exec :colorscheme true)
                             (string.gsub "-" "_"))]
          (vim.cmd {:cmd :AirlineTheme :args [theme-name]})))
  {})

(vim.api.nvim_create_user_command
  :PackerDo
  (fn []
    (vim.fn.delete (.. (vim.fn.stdpath :config) "/plugin/packer_compiled.lua"))
    (vim.cmd :PackerSync))
  {})
