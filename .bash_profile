. ~/.config/bash/env

BASH_ENV=

. ~/.config/bash/login

# 5. Run ~/.bash/interactive if this is an interactive shell.
if [ "$PS1" ]; then
    . ~/.config/bash/rc
fi

#	vim: ft=sh:tw=80
