This HOWTO contains information on how to configure certain aspects of an
install apart from creating config files and shell scripts.

# Swap capslock and escape
## localectl method
The following changes the system-wide keyboard configuration:

`localectl set-x11-keymap us "" "" caps:swapescape,terminate:ctrl_alt_bksp`
The double quotes denote an empty field in the `set-x11-keymap` option.

## setxkbmap method
The following changes only the user X11 keyboard configuration:

`setxkbmap -rules evdev -model pc104 -layout us,epo -option grp:ctrls_toggle -option caps:swapescape -option terminate:ctrl_alt_bksp`
These settings take effect immediately.

# Make menu key the Compose key
## setxkbmap method
`setxkbmap -option compose:menu`

# Change key repeat delay
## setx method
`xset r rate <delay_in_ms> <rate_in_ms>`
This only lasts as long as the session and does not persist between suspends.

## X server options
Edit lightdm config at /etc/lightdm/lightdm.conf:
Uncomment the `xserver-command=` line and add `-ardelay <delay_in_ms>` and or
`-arinterval <rate_in_ms>`
This is permanent for as long as X is running.

# Audio stuttering/hesitating
## Adjust sample rate
Adjust default-sample-rate in ~/.config/pulse/daemon.conf to 48000 from 44100
`default-sample-rate = 4800`
pulseaudio reports that the Scarlett 6i6 doesn't support 44100 sample rate
## Adjust default sample format
pulseaudio reports that the Scarlett 6i6 doesn't support s16le but s32le instead
`default-sample-format = s32le`
This seems to have had an effect.
## Adjust default fragments
`default-fragments = 5`
`default-fragment-size-msec = 2`
This doesn't seem to have an effect.
## Resampling
`avoid-resampling = true`
This doesn't seem to have an effect.
# JACK/Pulseaudio/Alsa
## Kernel Config
In `/etc/modprobe.d/alsa-base.conf` add the following:
    options snd_usb_audio index=0
    options snd_hda_intel index=1
In `/etc/modprobe.d/scarlett.conf` add the following:
    options snd_usb_audio vid=0x1235 pid=0x8203 device_setup=1
## alsamixer setting
Set Analogue 1 and 2 to PCM 01 and 02 respectively.
